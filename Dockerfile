# Use the rootproject/root image as the base image
FROM rootproject/root:latest

# Update package lists and install necessary packages
ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV NPROC=8

ENV LD_LIBRARY_PATH=/usr/local/lib

# APT Package Manager
COPY Dockerfile.packages packages

RUN apt-get update -qq && \
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime && \
    apt-get -y install $(cat packages | grep -v '#') wget git nano && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/cache/apt/archives/* && \
    rm -rf /var/lib/apt/lists/*

# Conda Environment Manager
COPY environment.yml .
COPY requirements.txt .
ENV PATH=/opt/conda/bin:$PATH

RUN wget -O Miniforge3.sh "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh" && \
    sh Miniforge3.sh -b -p "/opt/conda" && rm Miniforge3.sh && \
    mamba env create --file environment.yml && \
    mamba clean --all --yes

WORKDIR /root
CMD ["/bin/bash"]