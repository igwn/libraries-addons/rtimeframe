// Author:

/*************************************************************************
 * Copyright (C) 1995-2022, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_INTERNAL_RTF_RCOLUMNREADERREMAPPER
#define ROOT_INTERNAL_RTF_RCOLUMNREADERREMAPPER

#include <ROOT/RTF/RColumnReaderBase.hxx> 

#include <memory>
#include <Rtypes.h>

namespace ROOT {
namespace Detail {
namespace RTF {

template <class F>
class RColumnReaderRemapper : public ROOT::Detail::RTF::RColumnReaderBase {
private:
   std::unique_ptr<ROOT::Detail::RTF::RColumnReaderBase> fRedirectColumnReader;

   F fRemapper;

public:
   void *GetImpl(Long64_t entry) final { return fRedirectColumnReader->GetImpl(fRemapper(entry)); }

   RColumnReaderRemapper(std::unique_ptr<ROOT::Detail::RTF::RColumnReaderBase> redirectColumnReader, F remapper)
      : fRedirectColumnReader(std::move(redirectColumnReader)), fRemapper(remapper)
   {
   }
};

} // namespace RDF
} // namespace Detail
} // namespace ROOT

#endif
